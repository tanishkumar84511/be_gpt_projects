import openai

import settings

openai.api_key = settings.OPENAI_API_KEY
openai.organization = settings.OPENAI_ORG

conversation = []


def logic(sys_prompt, user_prompt):
    global conversation
    try:
        if sys_prompt is not None:
            conversation = [{
                "role": "system",
                "content": sys_prompt
            }]
        if user_prompt is not None:
            conversation.append({
                "role": "user",
                "content": user_prompt
            })
        response = openai.ChatCompletion.create(
            model=settings.COMPLETIONS_MODEL,
            messages=conversation,
            temperature=0.9,
            max_tokens=600,
            top_p=1,
            frequency_penalty=1.5,
            presence_penalty=1.5,
            stop=["\n**\n"]
        )

        return response['choices'][0]['message']['content']
    except Exception as e:
        print(e)
        return ""


def chatgpt():
    while True:
        # Default _sys_prompt:
        #     """
        #     You are an AI therapist who gives solid relationship advice to couples.
        #     I take things too seriously. How do I lighten up and handle small, daily issues better?
        #     """
        # You can send "na" as _sys_prompt after that
        _sys_prompt = input("Enter system prompt")
        if len(_sys_prompt) < 10:
            _sys_prompt = None
        _user_prompt = input("Enter user prompt")
        print(logic(_sys_prompt, _user_prompt))


chatgpt()
